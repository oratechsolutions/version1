import React, { Component } from 'react'
import {Route, Switch} from 'react-dom'
import Navbar from './partials/Navbar'
import Preloader from './partials/Preloader'
import Footer from './partials/Footer'
import Blog from './sections/Blog'
import Home from './sections/Home'
import About from './sections/About'
import Enquiries from './sections/Enquiries'
import FarmerBenefits from './sections/FarmerBenefits'
import VendorBenefits from './sections/VendorBenefits'
export class Landing extends Component {
    render() {
        return (
                <div>
<Preloader />
<Navbar />
  {/* HOME START */}
<Home />
  {/* HOME END */}
  {/* ABOUT START */}
<About />
  {/* ABOUT END */}

  {/* FARMER BENEFITS START */}
  <FarmerBenefits />
  {/* FARMER BENEFITS 1 END */}
    {/* VENDOR BENEFITS START */}
    <VendorBenefits />
  {/* VENDOR BENEFITS 1 END */}

<Blog />
  {/* CONTACT US START */}
<Enquiries />
  {/* CONTACT US END */}
 <Footer />
</div>
        )
    }
}

export default Landing
