import React, { Component } from 'react'
import logo from '../../assets/images/vika.png'
export default class Header extends Component {
    render() {
        return (
            <div>
                  {/*Navbar Start*/}
  <nav className="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
    <div className="container">
      {/* LOGO */}
      <a className="navbar-brand logo" href="/#">
        <img src={logo} alt="logo" height="80" />
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarCollapse"
        aria-controls="navbarCollapse"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i className="mdi mdi-menu" />
      </button>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav ml-auto navbar-center" id="mySidenav">
          <li className="nav-item active">
            <a href="/" className="nav-link">
              Home
            </a>
          </li>
          <li className="nav-item">
            <a href="/#about" className="nav-link">
              About
            </a>
          </li>
  
          <li className="nav-item">
            <a href="/#bFarmer" className="nav-link">
              Farmer Benefits
            </a>
          </li>
          <li className="nav-item">
            <a href="/#bVendor" className="nav-link">
              Vendor Benefits
            </a>
          </li>
          <li className="nav-item">
            <a href="/#blog" className="nav-link">
              Insights
            </a>
          </li>
          <li className="nav-item">
            <a href="/#contact" className="nav-link">
              Contact us
            </a>
          </li>

        </ul>
        {/* <button type="button" class="btn btn-custom-hover mr-4">Farmer SignUp</button> */}

      </div>
    </div>
  </nav>
  {/* Navbar End */}
            </div>
        )
    }
}
