import {FieldGroup, FieldControl} from 'react-reactive-form';
import React, { Component } from 'react';

import Values from './Values';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const required = value => (value ? undefined : "Required")



export default class Wizard extends Component {
    static Step = ({children}) => children
    constructor(props){
        super(props)
        this.state = {
            page : 0,
            values: props.initialValues 

        }
    }

    next = values =>
    this.setState(state =>({
      page : Math.min(state.page + 1, this.props.length - 1),
      values 
    }))
    previous = () =>
    this.setState(state => ({
      page: Math.max(state.page - 1, 0)

    }))

    validate = values => {
      const activePage = React.children.toArray(this.props.children)[
        this.state.page
      ]
      return activePage.props.validate ? activePage.props.validate(values) : {}
    }

    // goToNext = () =>{
    //     this.setState(prevState => ({
    //         step : Math.min(prevState.step + 1, this.props.children.length - 1)
    //     })) }
    // goToPrev =() =>{
    //     this.setState(prevState =>({
    //         step : Math.max(prevState.step -1, this.props.children.length -1, 0)
    //     })) }
  
        handleSubmit = (values, bag) => {
            const { children, onSubmit } = this.props;
            const { page } = this.state;
            const isLastPage = page === React.Children.count(children) - 1;

            if (isLastPage) {
            return onSubmit(values, bag);
            } else {
             this.next(values)
             bag.setSubmitting(false)
            }
          }
    render() {
        const {children} = this.props
        const {page, values} = this.state
        const activePage =  React.Children.toArray(children)[page]

        const isLastPage = page === React.Children.count(children) - 1
        

        return (
            <FieldGroup
            initialValues = {Values}
            enableReintialize = {false}
            validate = {this.validate}
            onSubmit = {this.handleSubmit}
            strict={false}
            options = {{ updateOn : 'submit'}}
            render = {props => (
                <form onSubmit={props.handleSubmit}>
                {React.cloneElement(activePage, {parentState: {...props}})}
<div className="buttons">
                    {page > 0 && (
                <button type="button" onClick={this.goToPrev}>Previous</button>)}

                
                {isLastPage && <button className="submitBnt btn btn-custom" type="submit">Submit</button>}
              {!isLastPage && <button className="submitBnt btn btn-custom" type="submit">Next</button>}
              <Values value={props.value} />

              </div>
              <pre>{JSON.stringify(values, null, 2)}</pre>
            </form>

            )
            }

            />
        )
    }
}

