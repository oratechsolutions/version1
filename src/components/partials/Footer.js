import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <div>
                 {/* FOOTER START */}
  <section className="footer-bg">
    <div className="container">
      <div className="row">
        <div className="col-lg-4">
          <div className="mb-5">
            <p className="text-uppercase text-dark footer-title mb-4">
              About Us
            </p>
            <p className="text-muted f-14">
              We are an integrated formal market place for small scale farmers.
            </p>
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-8">
          <div className="row">
            <div className="col-lg-4">
              <div className="mb-5">
                <p className="text-uppercase text-dark footer-title mb-4">
                  Vendor
                </p>
                <ul className="list-unstyled footer-sub-menu">
                  <li className="f-14">
                    <a href="#bVendor" className="text-muted">
Benefits                    </a>
                  </li>
                  <li className="f-14">
                    <a href className="text-muted">
Sign Up                    </a>
                  </li>

                </ul>
              </div>
            </div>
            {/* col end */}
            <div className="col-lg-4">
              <div className="mb-5">
                <p className="text-uppercase text-dark footer-title mb-4">
                  Farmer
                </p>
                <ul className="list-unstyled footer-sub-menu">
                  <li className="f-14">
                    <a href="#bFarmer" className="text-muted">
Benefits                    </a>
                  </li>
                  <li className="f-14">
                    <a href className="text-muted">
                      Sign Up
                    </a>
                  </li>
            
                </ul>
              </div>
            </div>
            {/* col end */}
            <div className="col-lg-4">
              <div className="mb-5">
                <p className="text-uppercase text-dark footer-title mb-4">
                  Insights
                </p>
                <ul className="list-unstyled footer-sub-menu">
                  <li className="f-14">
                    <a href className="text-muted">
                      Blog
                    </a>
                  </li>
                
                </ul>
              </div>
            </div>
            {/* col end */}
          </div>
          {/* row end */}
        </div>
        {/* col end */}
      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
  {/* FOOTER END */}
  {/* FOOTER ALT START */}
  <section className="footer-alt bg-dark pt-3 pb-3">
    <div className="container">
      <div className="row">
        <div className="col-lg-12 text-center">
          <p className="copyright text-white f-14 font-weight-light mb-0">
            Vikaboni. All Rights Reserved. 2020
          </p>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
            </div>
        )
    }
}

export default Footer
