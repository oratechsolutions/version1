import React, { Component } from 'react'
import Wizard from '../partials/Wizard'
import {FieldControl, Validators} from 'react-reactive-form'
import Header from '../partials/Header'
import Footer from '../partials/Footer'


const handleSubmit = control =>{
    alert(`you submitted \n ${JSON.stringify(control.value, null, 2)}`)
}
const Error = ({msg}) =>(
    <div>
        <span style ={StyleSheet.error}>{msg}</span>
    </div>
)

export default class Vendor extends Component {
    render() {
        return (
            <div>
              <Header />
                <section className="section bg-light" id="contact">
  <div className="container">
    <div className="row">
      <div className="col-lg-12">
        <div className="title-heading mb-5">
          <h3 className="text-dark mb-1 font-weight-light text-uppercase">Sign Up as A Vendor</h3>
          <div className="title-border-simple position-relative" />
        </div>
      </div>
      {/* col end */}
    </div>
    {/* row end */}
    <div className="row">
      <div className="col-lg-12">
        <div className="contact-box p-5">
          <div className="row">
            <div className="col-lg-12 col-md-12">
              <div className="custom-form p-3">
                <div id="message" />

                <Wizard onSubmit={handleSubmit}>
    {/* personal details */}
<Wizard.Step>

<div className="row">


      <FieldControl 
      strict= {false}
      name="firstName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="First Name" />
            { submitted &&
                hasError('required') && <Error msg="First name is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="lastName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="Last Name" />
            { submitted &&
                hasError('required') && <Error msg="Last name is required" />}
            </div>
            </div>   )}/>
{/* col end */}

<FieldControl 
      strict= {false}
      name="phone"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="tel" className="form-control" placeholder="Phone Number" />
            { submitted &&
                hasError('required') && <Error msg="Valid phone number is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="email"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="email" className="form-control" placeholder="Email Address" />
            { submitted &&
                hasError('required') && <Error msg="A valid email address is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="location"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="Location" />
            { submitted &&
                hasError('required') && <Error msg="Location is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="farmsize"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="number" className="form-control" placeholder="Farm Size" />
            { submitted &&
                hasError('required') && <Error msg="Farm size is required" />}
            </div>
            </div>   )}/>
{/* col end */}


</div>



</Wizard.Step>


<Wizard.Step>

</Wizard.Step>
</Wizard>




              </div>
            </div>
            {/* col end */}

          </div>
          {/* row end */}
        </div>
      </div>
      {/* col end */}
    </div>
    {/* row end */}
  </div>
  {/* container end */}
</section>
<Footer />


  

            </div>
        )
    }
}

