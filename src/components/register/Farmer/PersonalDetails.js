import React, { Component } from 'react'
import {FieldControl, Validators} from 'react-reactive-form'

import Error from '../Farmer'
export class PersonalDetails extends Component {
  render() {
    return (
      <div>
      <h3 className="text-custom1">Personal Details</h3>
      <div className="row">


      <FieldControl 
      strict= {false}
      name="firstName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="First Name" />
            { submitted &&
                hasError('required') && <Error msg="First name is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="lastName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="Last Name" />
            { submitted &&
                hasError('required') && <Error msg="Last name is required" />}
            </div>
            </div>   )}/>
{/* col end */}

<FieldControl 
      strict= {false}
      name="phone"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="tel" className="form-control" placeholder="Phone Number" />
            { submitted &&
                hasError('required') && <Error msg="Valid phone number is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="email"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="email" className="form-control" placeholder="Email Address" />
            { submitted &&
                hasError('required') && <Error msg="A valid email address is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="location"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="text" className="form-control" placeholder="Location" />
            { submitted &&
                hasError('required') && <Error msg="Location is required" />}
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="farmsize"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">

            <input {...handler()} type="number" className="form-control" placeholder="Farm Size" />
            { submitted &&
                hasError('required') && <Error msg="Farm size is required" />}
            </div>
            </div>   )}/>
{/* col end */}


</div>
  


      </div>
    )
  }
}

export default PersonalDetails
