import React, { Component } from 'react'
import {FieldControl, Validators} from 'react-reactive-form'

import {cats, categories} from './data'

import Error from '../Farmer'

const mainCats = Object.values(cats)
console.log(mainCats)
  export class ProductCategory extends Component {

    

  render() {

    
    return (
      <div>
      <h3 className="text-custom1">Product Category</h3>
      <div className="row">


      <FieldControl 
      strict= {false}
      name="firstName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Herbs</label>
            <select {...handler()} type="text" className="form-control">
            <option value="us" disabled>US</option>
            <option value="uk" disabled>UK</option>
            <option value="china" disabled>CHINA</option>

            </select>
            
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="lastName"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Select type(s) of herbs </label>

            <input {...handler()} type="text" className="form-control"/>
            </div>
            </div>   )}/>
{/* col end */}

<FieldControl 
      strict= {false}
      name="phone"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Fruits</label>
            <input {...handler()} type="tel" className="form-control" />
            
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="email"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Select type(s) of fruits </label>
            <input {...handler()} type="email" className="form-control"/>
            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="location"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Vegetables</label>

            <input {...handler()} type="text" className="form-control"/>

            </div>
            </div>   )}/>
{/* col end */}
<FieldControl 
      strict= {false}
      name="farmsize"
      options = {{ validators : Validators.required}}
      render = {({
          handler, hasError, submitted}) =>(
            <div className="col-lg-6">
            <div className="form-group app-label">
            <label className="text-custom">Select type(s) of vegetables </label>
            <input {...handler()} type="number" className="form-control" />
            
            </div>
            </div>   )}/>
{/* col end */}


</div>
  


      </div>
    )
  }
}

export default ProductCategory
