

export const categories = [
    {value: 'Herbs', label: 'Herbs & Spices'},
    {value: 'Vegetables', label: 'Vegetables'},
    {value: 'Fruits', label: 'Fruits'}
];

export const herbs = [
        {value : "basil", label : "Basil"},
        {value : "cinnamon", label : "Cinnamon"},
        {value : "pepper", label : "Pepper"},
        {value : "garlic", label : "Garlic"},
        {value : "ginger", label : "Ginger"},
        {value : "turmeric", label : "Turmeric"}
      
      
      ];
      export const vegetables = {
         leafy_green : [
            {value : "basil", label : "Basil"},
            {value : "cinnamon", label : "Basil"},
            {value : "pepper", label : "Pepper"},
            {value : "garlic", label : "Garlic"},
            {value : "ginger", label : "Ginger"},
            {value : "turmeric", label : "Turmeric"}
          ],
          cruciferous : [
            {value : "basil", label : "Basil"},
            {value : "cinnamon", label : "Basil"},
            {value : "pepper", label : "Pepper"},
            {value : "garlic", label : "Garlic"},
            {value : "ginger", label : "Ginger"},
            {value : "turmeric", label : "Turmeric"}
          ],
          marrow : [
            {value : "basil", label : "Basil"},
            {value : "cinnamon", label : "Basil"},
            {value : "pepper", label : "Pepper"},
            {value : "garlic", label : "Garlic"},
            {value : "ginger", label : "Ginger"},
            {value : "turmeric", label : "Turmeric"}
          ],
          root : [
            {value : "basil", label : "Basil"},
            {value : "cinnamon", label : "Basil"},
            {value : "pepper", label : "Pepper"},
            {value : "garlic", label : "Garlic"},
            {value : "ginger", label : "Ginger"},
            {value : "turmeric", label : "Turmeric"}
          ],
          allium : [
            {value : "basil", label : "Basil"},
            {value : "cinnamon", label : "Basil"},
            {value : "pepper", label : "Pepper"},
            {value : "garlic", label : "Garlic"},
            {value : "ginger", label : "Ginger"},
            {value : "turmeric", label : "Turmeric"}
          ]

      };

      
      export const fruits = {
        citrus_fruits : [
            {value : "oranges", label : "Oranges"},
            {value : "grapefruits", label : "Grapefruits"},
            {value : "mandarins", label : "Mandarins"},
            {value : "limes", label : "Limes"}
          ],
          exotic_tropical : [
            {value : "bananas", label : "Bananas"},
            {value : "mangoes", label : "Mangoes"}
          ],
          berries : [
            {value : "strawberries", label : "Strawberries"},
            {value : "blueberries", label : "Blueberries"},
            {value : "passion_fruit", label : "Passion Fruit"}
          ],
          melons : [
            {value : "watermelons", label : "Watermelons"}
          ] ,
          apples_pears : [
            {value : "apples", label : "Apples"},
            {value : "pears", label : "Pears"}
          ] ,
          tomatoes_avocados : [
            {value : "tomatoes", label : "Tomatoes"},
            {value : "avocadoes", label : "Avocadoes"}
          ] 
             }





    export const cats = [{vegetables:vegetables}, {herbs:herbs},{fruits:fruits}]

      

      





