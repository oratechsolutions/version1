import React, { Component } from 'react'
import Wizard from '../partials/Wizard'
import Footer from '../partials/Footer'
import {FieldControl, Validators} from 'react-reactive-form'
import Navbar  from '../partials/Navbar'

//components based 

// import PersonalDetails from './Farmer/PersonalDetails'
import ProductCategory from './Farmer/ProductCategory'


const handleSubmit = values =>{
    alert(`you submitted \n ${JSON.stringify(values.value, null, 2)}`)
}


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const Error = ({msg}) =>(
    <div>
        <span style ={StyleSheet.error}>{msg}</span>
    </div>
)
export default class Farmer extends Component {


 render() {
        return (
            <div>
              <Navbar />
                <section className="section bg-light" id="contact">
  <div className="container">
    <div className="row">
      <div className="col-lg-12">
        <div className="title-heading mb-5">
          <h3 className="text-dark mb-1 font-weight-light text-uppercase">Sign Up as A Farmer</h3>
          <div className="title-border-simple position-relative" />
        </div>
      </div>
      {/* col end */}
    </div>
    {/* row end */}
    <div className="row">
      <div className="col-lg-12">
        <div className="contact-box p-5">
          <div className="row">
            <div className="col-lg-12 col-md-12">
              <div className="custom-form p-3">
                <div id="message" />

                <Wizard onSubmit = {(values, actions ) => {
                  sleep(300).then(() =>{
                    window.alert(JSON.stringify(values, null, 2))
                    actions.setSubmitting(false)
                  })}
                  } >
    {/* personal details */}
<Wizard.Step>  
  <ProductCategory />
</Wizard.Step>

</Wizard>




              </div>
            </div>
            {/* col end */}

          </div>
          {/* row end */}
        </div>
      </div>
      {/* col end */}
    </div>
    {/* row end */}
  </div>
  {/* container end */}
</section>
<Footer />



  

            </div>
            
        )
    }

}
