import React, { Component } from 'react'

export class Home extends Component {
    render() {
        return (
            
                <div className="section home-2-bg" id="home">
  <div className="home-center">
    <div className="home-desc-center">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6">
            <div className="mt-40 home-2-content">
              <h1 className="text-white font-weight-normal home-2-title display-4 mb-0 text-custom1">
Linking Small Scale Farmers to Urban Markets              </h1>
              <h4 className="text-black-70 mt-4 f-15 mb-0">
              Vikaboni is a marketplace platform designed for the small scale farmer. It ensures that the farmer accesses formal markets helping both players do business with each other in a trusted environment.
              </h4>
              <div className="mt-5">
                <a href="/#" className="btn btn-custom mr-4">
                  Learn More
                </a>
              </div>
            </div>
          </div>
          {/* col end */}
          <div className="col-lg-6">
            <div className="mt-40 home-2-content position-relative">
        
            <img className="img-fluid mx-auto d-block home-2-img mover-img" src={require("../../assets/images/vendors.png")} alt="" />

              <div className="home-2-bottom-img">
              <img className="img-fluid d-block mx-auto" src={require("../../assets/images/homr-2-bg-bottom.png")} alt="" />

              </div>
            </div>
          </div>
          {/* col end */}
        </div>
        {/* row end */}
      </div>
      {/* container end */}
    </div>
  </div>

            </div>
        )
    }
}

export default Home
