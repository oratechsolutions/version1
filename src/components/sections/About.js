import React, { Component } from 'react'

export class About extends Component {
    render() {
        return (
            <div>
                  <section className="section bg-about" id="about">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="title-heading mb-5">
            <h3 className="text-white mb-1 font-weight-light text-uppercase">
What we do            </h3>
            <div className="title-border-color position-relative" />
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
      <div className="row">
        <div className="col-lg-4">
          <div className="about-box text-center p-3">
            <div className="about-icon mb-4">
              <i className="mdi mdi-lightbulb" />
            </div>
            <h4 className="font-weight-light">
              <a href="/#" className="text-white">
Quality Produce               </a>
            </h4>
            <p className="text-white-50 f-14">
              Nemo 
            </p>
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-4">
          <div className="about-box text-center p-3">
            <div className="about-icon mb-4">
              <i className="mdi mdi-projector-screen" />
            </div>
            <h4 className="font-weight-light">
              <a href="/#" className="text-white">
Reliable Market              </a>
            </h4>
            <p className="text-white-50 f-14">
              N
            </p>
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-4">
          <div className="about-box text-center p-3">
            <div className="about-icon mb-4">
              <i className="mdi mdi-nature" />
            </div>
            <h4 className="font-weight-light">
              <a href="/#" className="text-white">
Credit Access              </a>
            </h4>
            <p className="text-white-50 f-14">
              Nemo
            </p>
          </div>
        </div>
        {/* col end */}
      </div>

    </div>
    {/* container end */}
  </section>
            </div>
        )
    }
}

export default About
