import React, { Component } from 'react'

export class Clients extends Component {
    render() {
        return (
            <div>
                  {/* CLIENTS START */}
  <section className="section bg-clients" id="clients">
    <div className="bg-overlay" />
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="title-heading mb-5">
            <h3 className="text-white mb-1 font-weight-light text-uppercase">
              Our Clients
            </h3>
            <div className="title-border-color position-relative" />
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
      <div className="row">
        <div id="owl-demo">
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Senior Management"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                <img className="img-fluid" src={require('../assets/images/quote-img.png')} alt="" />  
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                <img className="img-fluid rounded-circle" src={require('../assets/images/clients/img-1.jpg')} alt="" />  
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Sherrie Barboza
                  </h6>
                  <p className="text-white-70 f-13 mb-0">UI/UX Designer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Web Development"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                  <img src="images/quote-img.png"  className="img-fluid" />
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                  <div className="client-img">
                    <img
                      src="images/clients/img-2.jpg"
                      
                      className="img-fluid rounded-circle"
                    />
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Jonas Davis
                  </h6>
                  <p className="text-white-70 f-13 mb-0">Web Designer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Graphic Developer"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                  <img src="images/quote-img.png"  className="img-fluid" />
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                  <div className="client-img">
                    <img
                      src="images/clients/img-3.jpg"
                      
                      className="img-fluid rounded-circle"
                    />
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Mary Cantu
                  </h6>
                  <p className="text-white-70 f-13 mb-0">PHP Developer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Best Designer"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                  <img src="images/quote-img.png"  className="img-fluid" />
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                  <div className="client-img">
                    <img
                      src="images/clients/img-4.jpg"
                      
                      className="img-fluid rounded-circle"
                    />
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Rodney Grey
                  </h6>
                  <p className="text-white-70 f-13 mb-0">Graphic Designer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Senior Management"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                  <img src="images/quote-img.png"  className="img-fluid" />
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                  <div className="client-img">
                    <img
                      src="images/clients/img-5.jpg"
                      
                      className="img-fluid rounded-circle"
                    />
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Shana Esposito
                  </h6>
                  <p className="text-white-70 f-13 mb-0">Web Developer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
          <div className="item">
            <div className="testi-content">
              <div className="testi-box mt-4">
                <h4 className="text-white mb-3 font-weight-light">
                  "Graphic Developer"
                </h4>
                <p className="text-white-70 font-weight-light mb-0 f-15">
                  At vero eos et accusamus et iusto odio dignissimos that site
                  ducimus qui blanditiis praesentium voluptatum deleniti atque
                  corrupti quos dolores quas molestias excepturi cupiditate non.
                </p>
                <div className="quote-img">
                  <img src="images/quote-img.png"  className="img-fluid" />
                </div>
              </div>
              <div className="mt-2">
                <div className="float-right ml-3">
                  <div className="client-img">
                    <img
                      src="images/clients/img-6.jpg"
                      
                      className="img-fluid rounded-circle"
                    />
                  </div>
                </div>
                <div className="clients-name text-right pt-3">
                  <h6 className="text-white font-weight-normal position-relative f-17 mb-0">
                    <span className="after-border" /> Linda Sanor
                  </h6>
                  <p className="text-white-70 f-13 mb-0">UI/UX Designer</p>
                </div>
              </div>
            </div>
          </div>
          {/* owl item and */}
        </div>
      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
  {/* CLIENTS END */}
            </div>
        )
    }
}

export default Clients
