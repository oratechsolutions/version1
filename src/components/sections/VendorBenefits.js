import React, { Component } from 'react'

export default class VendorBenefits extends Component {
    render() {
        return (
            <div>
                <section className="section bg-features" id="bVendor">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="title-heading mb-5">
            <h3 className="text-dark mb-1 font-weight-light text-uppercase">
              Vendor Benefits
            </h3>
            <div className="title-border-simple position-relative" />
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
      <div className="row align-items-center">
      <div className="col-lg-6">
          <div className="features-img mt-32">
            <img className="img-fluid mx-auto d-block" src={require('../../assets/images/vendors.png')} alt="" />  
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-6">
          <div className="features-content">
            <div className="features-icon">
              <i className="pe-7s-science" />
            </div>
            <h3 className="text-muted f-10">
              As a vendor, you will be able to enjoy the following benefits
            </h3>
            <h5 className="font-weight-normal text-dark mb-3 mt-4">
            Access to Quality Produce.            </h5>
            <h5 className="font-weight-normal text-dark mb-3 mt-4">
            Guaranteed consistent supply of produce.         </h5>
            <h5 className="font-weight-normal text-dark mb-3 mt-4">
            Flexible Payment model.            </h5>
            <h5 className="font-weight-normal text-dark mb-3 mt-4">
            Convenience, less hustle in sourcing for produce.            </h5><br />
            <p className="mb-0 text-uppercase f-13">
              <a href="/vendor" className="btn btn-custom mr-4">
                Sign Up as Vendor <span className="right-arrow ml-1">⇾</span>
              </a>
            </p>
          </div>
        </div>
        {/* col end */}

      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
            </div>
        )
    }
}

