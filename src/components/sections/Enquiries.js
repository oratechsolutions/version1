import React, { Component } from 'react'

export class Enquiries extends Component {
    render() {
        return (
            <div>
                  <section className="section bg-light" id="contact">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="title-heading mb-5">
            <h3 className="text-dark mb-1 font-weight-light text-uppercase">
              Get in touch
            </h3>
            <div className="title-border-simple position-relative" />
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
      <div className="row">
        <div className="col-lg-12">
          <div className="contact-box p-5">
            <div className="row">
              <div className="col-lg-8 col-md-6">
                <div className="custom-form p-3">
                  <div id="message" />
                  <form
                    method="post"
                    action="/#"
                    name="contact-form"
                    id="contact-form"
                  >
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group app-label">
                          <input
                            name="name"
                            id="name"
                            type="text"
                            className="form-control"
                            placeholder="Name"
                          />
                        </div>
                      </div>
                      {/* col end */}
                      <div className="col-lg-6">
                        <div className="form-group app-label">
                          <input
                            name="email"
                            id="email"
                            type="email"
                            className="form-control"
                            placeholder="Email"
                          />
                        </div>
                      </div>
                      {/* col end */}
                      <div className="col-lg-12">
                        <div className="form-group app-label">
                          <input
                            type="text"
                            className="form-control"
                            id="subject"
                            placeholder="Subject"
                          />
                        </div>
                      </div>
                      {/* col end */}
                      <div className="col-lg-12">
                        <div className="form-group app-label">
                          <textarea
                            name="comments"
                            id="comments"
                            rows={5}
                            className="form-control"
                            placeholder="Message"
                            defaultValue={""}
                          />
                        </div>
                      </div>
                      {/* col end */}
                    </div>
                    {/* row end */}
                    <div className="row">
                      <div className="col-sm-12">
                        <input
                          type="submit"
                          id="submit"
                          name="send"
                          className="submitBnt btn btn-custom"
                          defaultValue="Send Message"
                        />
                        <div id="simple-msg" />
                      </div>
                      {/* col end */}
                    </div>
                    {/* row end */}
                  </form>
                </div>
              </div>
              {/* col end */}
              <div className="col-lg-4 col-md-6">
                <div className="contact-cantent p-3">
                  <div className="contact-details">
                    <div className="float-left contact-icon mr-3 mt-2">
                      <i className="mdi mdi-headphones text-muted h5" />
                    </div>
                    <div className="app-contact-desc text-muted pt-1">
                      <h4 className="mb-0 info-title f-13">Call :</h4>
                      <p className="mb-0 f-13">0100 959961</p>
                    </div>
                  </div>
                  <br />
                  <div className="contact-details mt-2">
                    <div className="float-left contact-icon mr-3 mt-2">
                      <i className="mdi mdi-email-outline text-muted h5" />
                    </div>
                    <div className="app-contact-desc text-muted pt-1">
                      <h4 className="mb-0 info-title f-13 text-custom1">Farmer  :</h4>
                      <p className="mb-0 f-13">
                        <a href className="text-muted">
                           farmer@vikaboni.com
                        </a>
                      </p>
                    </div>
                  </div>
                  <br />
                  <div className="contact-details mt-2">
                    <div className="float-left contact-icon mr-3 mt-2">
                      <i className="mdi mdi-map-marker text-muted h5" />
                    </div>
                    <div className="app-contact-desc text-muted pt-1">
                      <h4 className="mb-0 info-title f-13  text-custom">Vendor :</h4>
                      <p className="mb-0 f-13">
                        <a href className="text-muted">
                        vendor@vikaboni.com
                        </a>
                      </p>
                    </div>
                  </div>
                  <br />
                  <div className="follow mt-4">
                    <h4 className="text-dark mb-3">Follow</h4>
                    <ul className="follow-icon list-inline mt-32 mb-0">
                      <li className="list-inline-item f-15">
                        <a href="https://web.facebook.com/Vikaboni.ke/?_rdc=1&_rdr" className="social-icon text-muted">
                          <i className="mdi mdi-facebook" />
                        </a>
                      </li>
                      <li className="list-inline-item f-15">
                        <a href="https://twitter.com/Vikaboni_Ke" className="social-icon text-muted">
                          <i className="mdi mdi-twitter" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* col end */}
            </div>
            {/* row end */}
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
            </div>
        )
    }
}

export default Enquiries
