import React, { Component } from 'react'

export class Blog extends Component {
    render() {
        return (
            <div>
                  {/* BLOG START */}
  <section className="section" id="blog">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="title-heading mb-5">
            <h3 className="text-dark mb-1 font-weight-light text-uppercase">
Our Insights             </h3>
            <div className="title-border-simple position-relative" />
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
      <div className="row">
        <div className="col-lg-4">
          <div className="blog position-relative">
            <div className="blog-img position-relative mt-4">
              <div className="blog-date">
                <p className="mb-0 font-weight-light text-white f-15">
                  01 Jan 2019
                </p>
              </div>
              <img className="img-fluid mx-auto d-block rounded"

                src={require("../../assets/images/blog/img-1.jpg")} alt=""
                
              />
            </div>
            <div className="position-relative">
              <div className="blog-content text-center bg-white p-4">
                <p className="text-uppercase f-13 mb-2 text-muted">
                  ui/ux designer
                </p>
                <h5 className="font-weight-normal f-18">
                  <a href="/#" className="text-dark">
                    Quis autem reprehenderit
                  </a>
                </h5>
                <p className="text-muted f-14">
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui
                  blanditiis deleniti corrupti.
                </p>
                <div className="read-more">
                  <a href="/#" className=" text-primary f-15">
                    Read more
                    <i className="mdi mdi-arrow-right" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-4">
          <div className="blog position-relative">
            <div className="blog-img position-relative mt-4">
              <div className="blog-date">
                <p className="mb-0 font-weight-light text-white f-15">
                  02 Jan 2019
                </p>
              </div>
              <img className="img-fluid mx-auto d-block rounded" src={require("../../assets/images/blog/img-2.jpg")} alt=""

 />
            </div>
            <div className="position-relative">
              <div className="blog-content text-center bg-white p-4">
                <p className="text-uppercase f-13 mb-2 text-muted">
                  web developer
                </p>
                <h5 className="font-weight-normal f-18">
                  <a href="/#" className="text-dark">
                    At vero eos accusamus
                  </a>
                </h5>
                <p className="text-muted f-14">
                  Et harum quidem rerum it facilis est et expedita distinctio a
                  libero tempore cumsoluta.
                </p>
                <div className="read-more">
                  <a href="/#" className=" text-primary f-15">
                    Read more
                    <i className="mdi mdi-arrow-right" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* col end */}
        <div className="col-lg-4">
          <div className="blog position-relative">
            <div className="blog-img position-relative mt-4">
              <div className="blog-date">
                <p className="mb-0 font-weight-light text-white f-15">
                  03 Jan 2019
                </p>
              </div>
              <img className="img-fluid mx-auto d-block rounded" src={require("../../assets/images/blog/img-3.jpg")} alt=""
                  />
            </div>
            <div className="position-relative">
              <div className="blog-content text-center bg-white p-4">
                <p className="text-uppercase f-13 mb-2 text-muted">
                  web designer
                </p>
                <h5 className="font-weight-normal f-18">
                  <a href="/#" className="text-dark">
                    Et harum quidem rerum
                  </a>
                </h5>
                <p className="text-muted f-14">
                  Temporibus autemes quibusdam et aut offici debitis rerum
                  necessitatibus recusandae.
                </p>
                <div className="read-more">
                  <a href="/#" className=" text-primary f-15">
                    Read more
                    <i className="mdi mdi-arrow-right" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* col end */}
      </div>
      {/* row end */}
    </div>
    {/* container end */}
  </section>
  {/* BLOG END */}
            </div>
        )
    }
}

export default Blog
